from django.contrib import admin
from .models import Shoe, ShoeVO

# Register your models here.
@admin.register(Shoe)
class ShoeAdmin(admin.ModelAdmin):
    pass

@admin.register(ShoeVO)
class ShoeVOAdmin(admin.ModelAdmin):
    pass
