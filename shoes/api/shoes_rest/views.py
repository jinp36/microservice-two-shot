from django.shortcuts import render
from common.json import ModelEncoder
from .models import Shoe, ShoeVO
from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods

# Create your views here.


class ShoeVOEncoder(ModelEncoder):
    model = ShoeVO
    properties = ["import_href", "closet_name"]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin_location",
        "id",
    ]
    encoders = {
        "bin_location": ShoeVOEncoder(),
    }


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
    ]

@require_http_methods(["GET", "POST"])
def api_list_shoes(request, shoe_vo_id=None):
    if request.method == "GET":

        if shoe_vo_id is not None:
            shoe = Shoe.objects.filter(bin_location=shoe_vo_id)
        else:
            shoe = Shoe.objects.all()
        return JsonResponse(
            {"shoe": shoe},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            bin_href = f"/api/bins/{content['bin_location']}/"
            shoe = ShoeVO.objects.get(import_href=bin_href)
            content["bin_location"] = shoe
        except ShoeVO.DoesNotExist:
            return JsonResponse(
                {"message": "does not exist"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeListEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_shoes(request, pk):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse(

            {"message": "Shoe not found"},
            status=404,
            )
    elif request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False
            )
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"}
            )
    else:
        content = json.loads(request.body)
        Shoe.objects.filter(id=pk).update(**content)
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
