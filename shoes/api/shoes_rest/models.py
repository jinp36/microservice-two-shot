from django.db import models
from django.urls import reverse

# Create your models here.


class ShoeVO (models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200, default=None)


class Shoe (models.Model):
    manufacturer = models.CharField(max_length=200)
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)

    bin_location = models.ForeignKey(
        ShoeVO,
        related_name="shoe",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_show_shoes", kwargs={"pk": self.pk})
