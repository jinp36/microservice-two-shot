from django.urls import path
from .views import api_hats, api_show_hat

urlpatterns = [

    path("locations/<int:LocVO_id>/hats/", api_hats, name="api_hats"),
    path("hat/<int:id>", api_show_hat, name="api_show_hat"),
]
