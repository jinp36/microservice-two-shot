from django.contrib import admin
from .models import Hats, LocVO

# Register your models here.
@admin.register(Hats)
class HatsAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "fabric",
        "style",
        "color",
        "picture_url",
        "loc",
    )

# Register your models here.
@admin.register(LocVO)
class LocVOAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number",

    )
