from django.shortcuts import render

# Create your views here.
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Hats, LocVO
from common.json import ModelEncoder

class LocVOEncoder(ModelEncoder):
    model = LocVO
    properties = [
        'import_href',
        'closet_name',
        'section_number',
        'shelf_number',
    ]

class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "id",
        "fabric",
        "style",
        "color",
        "picture_url",
        # "loc",
    ]
    # encoders = {
    #     'loc': LocVOEncoder,
    # }

class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style",
        "color",
        "picture_url",
    ]
    encoders = {
        'loc': LocVOEncoder,
    }


# Create your views here.
@require_http_methods(["GET", "POST"])
def api_hats(request, LocVO_id):

    if request.method == "GET":
        hats = Hats.objects.filter(loc=LocVO_id)
        return JsonResponse(
            {"hats": hats},
            encoder=HatsListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            loc_href = f'/api/locations/{LocVO_id}/'
            loc = LocVO.objects.get(import_href=loc_href)
            content["loc"] = loc
        except LocVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid loc id"},
                status=400
            )

        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatsListEncoder,
            safe=False,
            )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_hat(request, id):

    if request.method == "GET":
        try:
            hat = Hats.objects.get(id=id)
            return JsonResponse(
                hat,
                encoder=HatsDetailEncoder,
                safe=False
            )
        except hat.DoesNotExist:
            response = JsonResponse({"message": "Hat does not exist"})
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        count, _ = Hats.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        content = json.loads(request.body)

        Hats.objects.filter(id=id).update(**content)

        hat = Hats.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False,
        )
