import React from "react";

function ShoesList(props) {
    return (
    <table className="table table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>manufacturer</th>
                <th>Color</th>
                <th>picture</th>
                <th>bin location</th>
            </tr>
        </thead>
        <tbody>
        {props.shoe.map(shoes => {
            return (
                <tr key={shoes.href}>
                    <td>{shoes.model_name}</td>
                    <td>{shoes.manufacturer}</td>
                    <td>{shoes.color}</td>
                    <td>{shoes.picture_url}</td>
                    <td>{shoes.bin_location}</td>
                  </tr>
            );
        })}
        </tbody>
    </table>

    )
}

export default ShoesList;
