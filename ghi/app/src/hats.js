import React, {useState } from 'react';

export default function Hats() {

    const [formData, setFormData] = useState({
        fabric: '',
        style: '',
        color: '',
        picture_url: ''
        });

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    };

    


    const handleSubmit = async (e) => {
        e.preventDefault();

        const hatsUrl = 'http://localhost:8090/api/hats/';

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            }
        };

        const response = await fetch (hatsUrl, fetchConfig);

        if (response.ok) {
            setFormData({
            fabric: '',
            style: '',
            color: '',
            picture_url: ''
            });
        }
    };

    return (
        <div className="row">
            <div className="offset-3 col-6 mb-5 mt-5">
                <div className="shadow p-4 mt-3">
                    <h1 className='text-center'>Add Hat</h1>
                    <form onSubmit={handleSubmit} id="add-hat">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.fabric} placeholder='Name' required type="text" name="fabric" id="fabric" className="form-control" />
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.style} placeholder='style' required type="text" name="style" id="style" className="form-control" />
                            <label htmlFor="style">Style</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.color} placeholder='color' required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.picture_url} placeholder='picture_url' required type="text" name="picture_url" id="picture_url" className="form-control" />
                            <label htmlFor="picture_url">Picture Url</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
