import React, {useState,useEffect} from 'react';

function ShoeForm() {

    const [bin_locations,setBinLocations] = useState([]);

    const [modelName, setModelName] = useState('');
    const handleModelChange = (event) => {
        const value = event.target.value;
        setModelName(value);
    }
    const [manufacturer ,setManufacturer] = useState('');
    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }
    const [color, setColor] = useState('');
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const [picture_url, setPictureUrl ] = useState('');
    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const [bin_location,setBinLocation] = useState('');
    const handleBinLocationChange = (event) => {
        const value = event.target.value;
        setBinLocation(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.model_name = modelName;
        data.manufacturer = manufacturer;
        data.color = color;
        data.picture_url = picture_url;
        data.bin_location = bin_location;

    const url = 'http://localhost:8000/api/shoes'

    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(url,fetchConfig);
    if(response.ok){
        const newShoe = await response.json();
        console.log(newShoe);

        setModelName('');
        setManufacturer('');
        setColor('');
        setPictureUrl('');
        setBinLocation('');
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBinLocations(data.bin_locations);
        }
        }
        useEffect(() => {
            fetchData();
        }, []);

    return(
        <div>Hello World!</div>
    )

}
export default ShoeForm
