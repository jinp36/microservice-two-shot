import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import Hats from './hats';
import ShoesList from './ShoesList';
import ShoeForm from './ShoeForm';

function App(props) {
  // if (props.shoe === undefined) {
  //   return "hello";
  // }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats" element={<Hats />} />
          <Route path="shoes" element={<ShoesList shoes={props.shoe} />} />
          <Route path="shoes/new" element={<ShoeForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
